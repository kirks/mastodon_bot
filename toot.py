#!/usr/bin/env python3

"""Various Toot help method for bots."""

from mastodon import Mastodon


def toot(config, article=None, username=None, in_reply_to=None, mentions=None):
    """Send a toot."""
    # Setup Mastodon API
    mastodon = Mastodon(
        client_id=config['config']['client_cred_file'],
        access_token=config['config']['user_cred_file'],
        api_base_url=config['config']['api_base_url']
    )

    # Process main toot
    cw_text = None
    if 'cw_text' in config['toot']:
        cw_text = config['toot']['cw_text']
        # Replace RSS variables if they are present
        if article is not None:
            cw_text = cw_text.replace('[[ title ]]', article.title)
            cw_text = cw_text.replace('[[ url ]]', article.link)
            cw_text = cw_text.replace('[[ published ]]', article.published)
        # Replace username variable if it's present
        if username is not None:
            cw_text = cw_text.replace('[[ username ]]', username)

    toot_text = config['toot']['toot_text']
    # Replace RSS varaibles if they are present
    if article is not None:
        toot_text = toot_text.replace('[[ title ]]', article.title)
        toot_text = toot_text.replace('[[ url ]]', article.link)
        toot_text = toot_text.replace('[[ published ]]', article.published)

    # Replace username variable if it's present
    if username is not None:
        toot_text = toot_text.replace('[[ username ]]', username)

    if mentions is not None:
        mentions_text = ''
        for account in mentions:
            mentions_text = mentions_text + ' @' + account['acct']
            mentions_text = mentions_text.strip()
        toot_text = toot_text.replace('[[ mentions ]]', mentions_text)

    # Send main toot
    parent_toot = mastodon.status_post(toot_text, visibility=config['toot']['visibility'],
                                       spoiler_text=cw_text, in_reply_to_id=in_reply_to)
    parent_toot_id = parent_toot['id']

    # Handle any sub-toots
    if 'subtoots' in config['toot']:
        for subtoot in config['toot']['subtoots']:
            cw_text = None
            if 'cw_text' in subtoot:
                cw_text = subtoot['cw_text']

            mastodon.status_post(
                subtoot['toot_text'],
                in_reply_to_id=parent_toot_id,
                visibility=subtoot['visibility'],
                spoiler_text=cw_text
            )

    # Return the parent toot dict just in case it's needed elsewhere
    return parent_toot
