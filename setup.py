"""mastodon_bot installer."""

from setuptools import setup

setup(
    name='mastodon_bot',
    version='0.2.0',
    description='Various bots for Mastodon instances (see example configs)',
    url='https://gitlab.com/photog.social/mastodon_bot',
    author='KemoNine',
    author_email='mcrosson_mastobot@kemonine.info',
    license='GPLv3',
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)
